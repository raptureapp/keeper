<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Permission Driver
    |--------------------------------------------------------------------------
    |
    | This option controls the default permission driver that will be used to
    | authorize actions.
    |
    */

    'driver' => 'always',

];

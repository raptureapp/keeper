<?php

namespace Rapture\Keeper;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Rapture\Hooks\Facades\Hook;
use Rapture\Keeper\Facades\Keeper;
use Rapture\Keeper\Models\Permission;

class KeeperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->publishes([
            __DIR__ . '/../config/keeper.php' => config_path('keeper.php'),
        ], 'config');

        if (!$this->app->runningInConsole() && Schema::hasTable('permissions')) {
            $this->registerPermissions();
        }

        Hook::attach('package.disabled', 'Rapture\Keeper\Listeners\DisablePermissions');
        Hook::attach('package.enabled', 'Rapture\Keeper\Listeners\EnablePermissions');
        Hook::attach('package.uninstalled', 'Rapture\Keeper\Listeners\DeletePermissions');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/keeper.php', 'keeper');

        $this->app->singleton('keeper', function ($app) {
            return new PermissionManager($app);
        });

        $this->app->singleton('keeper.driver', function ($app) {
            return $app['keeper']->driver();
        });
    }

    public function registerPermissions()
    {
        $permissions = Permission::where('active', true)->pluck('keyname');

        $permissions->each(function ($item) {
            Gate::define($item, function ($user) use ($item) {
                return Keeper::allows($user, $item);
            });
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['keeper', 'keeper.driver'];
    }
}

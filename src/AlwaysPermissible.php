<?php

namespace Rapture\Keeper;

use Rapture\Keeper\Contracts\Permissible;

class AlwaysPermissible implements Permissible
{
    /**
     * Resolve a permission check
     *
     * @param  object  $user
     * @param  string  $permission
     * @return bool
     */
    public function allows($user, $permission)
    {
        return true;
    }
}

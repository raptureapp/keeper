<?php

namespace Rapture\Keeper;

use Illuminate\Support\Manager;
use Rapture\Keeper\Contracts\Permissible;

class PermissionManager extends Manager implements Permissible
{
    /**
     * Create an instance of the Always Permissible Driver.
     *
     * @return AlwaysPermissible
     */
    public function createAlwaysDriver()
    {
        return new AlwaysPermissible();
    }

    /**
     * Resolve a permission check
     *
     * @param  object  $user
     * @param  string  $permission
     * @return bool
     */
    public function allows($user, $permission)
    {
        return $this->driver()->allows($user, $permission);
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->container['config']['keeper.driver'] ?? 'always';
    }
}

<?php

namespace Rapture\Keeper\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    protected $key = '';

    public function viewAny(User $user)
    {
        return $user->can($this->key . '.index');
    }

    public function view(User $user, $model)
    {
        return $user->can($this->key . '.index');
    }

    public function create(User $user)
    {
        return $user->can($this->key . '.create');
    }

    public function update(User $user, $model)
    {
        return $user->can($this->key . '.edit');
    }

    public function delete(User $user, $model)
    {
        return $user->can($this->key . '.destroy');
    }
}

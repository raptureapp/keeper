<?php

namespace Rapture\Keeper\Listeners;

use Illuminate\Support\Facades\Artisan;
use Rapture\Keeper\Models\Permission;

class EnablePermissions
{
    public function handle($package)
    {
        Permission::where('package', $package)->update([
            'active' => true,
        ]);
    }
}

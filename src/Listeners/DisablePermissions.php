<?php

namespace Rapture\Keeper\Listeners;

use Illuminate\Support\Facades\Artisan;
use Rapture\Keeper\Models\Permission;

class DisablePermissions
{
    public function handle($package)
    {
        Permission::where('package', $package)->update([
            'active' => false,
        ]);
    }
}

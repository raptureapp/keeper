<?php

namespace Rapture\Keeper\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = [];

    public function groupName()
    {
        return strtoupper(implode(' - ', explode('.', $this->group)));
    }
}

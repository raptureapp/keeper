<?php

namespace Rapture\Keeper\Contracts;

interface Permissible
{
    /**
     * Resolve a permission check
     *
     * @param  object  $user
     * @param  string  $permission
     * @return bool
     */
    public function allows($user, $permission);
}

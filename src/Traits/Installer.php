<?php

namespace Rapture\Keeper\Traits;

use Rapture\Keeper\Models\Permission;

trait Installer
{
    public function registerPermission($group, $action, $description)
    {
        $latest = Permission::where([
            'package' => $this->name,
            'group' => $group,
        ])->orderBy('priority', 'desc')->first();

        return Permission::updateOrCreate([
            'package' => $this->name,
            'keyname' => $group . '.' . $action,
            'group' => $group,
        ], [
            'active' => true,
            'description' => $description,
            'priority' => $latest ? $latest->priority + 1 : 0,
        ]);
    }

    public function permissionGroup($group, $permissions = [])
    {
        $latest = Permission::where([
            'package' => $this->name,
            'group' => $group,
        ])->orderBy('priority', 'desc')->first();

        $index = $latest ? $latest->priority + 1 : 0;

        foreach ($permissions as $permisison => $description) {
            Permission::updateOrCreate([
                'package' => $this->name,
                'keyname' => $group . '.' . $permisison,
                'group' => $group,
            ], [
                'active' => true,
                'description' => $description,
                'priority' => $index,
            ]);

            $index++;
        }
    }

    public function resourcePermissions($group)
    {
        $this->permissionGroup($group, [
            'index' => 'rapture::permission.index',
            'create' => 'rapture::permission.create',
            'edit' => 'rapture::permission.edit',
            'destroy' => 'rapture::permission.destroy',
        ]);
    }
}

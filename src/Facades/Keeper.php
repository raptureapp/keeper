<?php

namespace Rapture\Keeper\Facades;

use Illuminate\Support\Facades\Facade;

class Keeper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'keeper';
    }
}
